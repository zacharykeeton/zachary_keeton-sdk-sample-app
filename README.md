# zachary_keeton-SDK-sample-app

A sample app exercising the `zachary_keeton-sdk` npm package here:

It is an SDK for the Lord of the Rings API here: https://the-one-api.dev/

To exercise it fully, sign up for a free API access key here: https://the-one-api.dev/sign-up

## Getting started

```bash
git clone git@gitlab.com:zacharykeeton/zachary_keeton-sdk-sample-app.git
touch .env
# add single line to .env file: API_KEY="<yourAccessKey>"
npm install
npm start
```

## API

See [app.js](https://gitlab.com/zacharykeeton/zachary_keeton-sdk-sample-app/-/blob/main/app.js) for examples
