const dotenv = require("dotenv");
dotenv.config();

// NOTE: create a `.env` file at the root-level and your API key
// on a line like so:
// API_KEY="<yourAccessKey>"

const {
  OneAPIClient,
  SORT_DIRECTION,
  FILTER_OPTIONS,
} = require("zachary_keeton-sdk");
const client = new OneAPIClient({ apiKey: process.env.API_KEY });

/// WARNING:  commenting in everyting below at one time risks hitting
/// the API's rate limit.

////
// BOOKS
////

//// Find all books
//////
client.books.find().then(console.log);

//// Find books with pagination, sorting, filtering
//////
// const options = {
//   pagination: {
//     page: 2,
//     limit: 1,
//     offset: 1,
//   },
//   sort: { name: "desc" },
//   filter: {
//     key: "name",
//     option: "NOT_MATCH_ANY",
//     expression: "zach",
//   },
// };
// client.books.find(options).then(console.log);

//// Find a book by id
////// client.books.findOne("5cf58080b53e011a64671584").then(console.log);

////
// MOVIES
////

//// Find all movies
//////
// client.movies.find().then(console.log);

//// Find movies with pagination, sorting, filtering
//////
// const options = {
//   pagination: {
//     page: 2,
//     limit: 1,
//     offset: 1,
//   },
//   sort: { name: "desc" },
//   filter: {
//     key: "name",
//     option: "NOT_MATCH_ANY",
//     expression: "zach",
//   },
// };
// client.movies.find(options).then(console.log);

//// Find a movie by id
////// client.movies.findOne("5cd95395de30eff6ebccde58").then(console.log);

////
// QUOTES
////

//// Find all quotes
//////
// client.quotes.find().then(console.log);

//// Find quotes with pagination, sorting, filtering
//////
// const options = {
//   pagination: {
//     page: 2,
//     limit: 1,
//     offset: 1,
//   },
//   sort: { name: "desc" },
//   filter: {
//     key: "name",
//     option: "NOT_MATCH_ANY",
//     expression: "zach",
//   },
// };
// client.quotes.find(options).then(console.log);

//// Find a quote by id
////// client.quotes.findOne("5cd96e05de30eff6ebcce7e9").then(console.log);

//// Find a quotes by character id
////// client.quotes.findByCharacter("5cd99d4bde30eff6ebccfbe6").then(console.log);

////
// CHAPTERS
////

//// Find all chapters
//////
// client.chapters.find().then(console.log);

//// Find chapters with pagination, sorting, filtering
//////
// const options = {
//   pagination: {
//     page: 2,
//     limit: 1,
//     offset: 1,
//   },
//   sort: { name: "desc" },
//   filter: {
//     key: "name",
//     option: "NOT_MATCH_ANY",
//     expression: "zach",
//   },
// };
// client.chapters.find(options).then(console.log);

//// Find a chapter by id
////// client.chapters.findOne("6091b6d6d58360f988133b8d").then(console.log);

//// Find a chapters by book id
////// client.chapters.findByBook("5cf58080b53e011a64671584").then(console.log);

////
// CHARACTERS
////

//// Find all characters
//////
// client.characters.find().then(console.log);

//// Find characters with pagination, sorting, filtering
//////
// const options = {
//   pagination: {
//     page: 2,
//     limit: 1,
//     offset: 1,
//   },
//   sort: { name: "desc" },
//   filter: {
//     key: "name",
//     option: "NOT_MATCH_ANY",
//     expression: "zach",
//   },
// };
// client.characters.find(options).then(console.log);

//// Find a character by id
////// client.characters.findOne("5cd99d4bde30eff6ebccfbe6").then(console.log);
